#!/bin/bash

KLIPPER_PATH="${HOME}/klipper"
CREATOR_MK3_PATH="${HOME}/creator-mk3s"

set -eu
export LC_ALL=C


function preflight_checks {
    if [ "$EUID" -eq 0 ]; then
        echo "[PRE-CHECK] This script must not be run as root!"
        exit -1
    fi

    if [ "$(sudo systemctl list-units --full -all -t service --no-legend | grep -F 'klipper.service')" ]; then
        printf "[PRE-CHECK] Klipper service found! Continuing...\n\n"
    else
        echo "[ERROR] Klipper service not found, please install Klipper first!"
        exit -1
    fi
}

function check_download {
    local dirname basename
    dirname="$(dirname ${CREATOR_MK3_PATH})"
    basename="$(basename ${CREATOR_MK3_PATH})"

    if [ ! -d "${CREATOR_MK3_PATH}" ]; then
        echo "[DOWNLOAD] Downloading Creator-MK3 repository..."
        if git -C $dirname clone https://gitlab.com/creator-makerspace/creator-mk3s-config.git $basename; then
            chmod +x ${CREATOR_MK3_PATH}/install.sh
            printf "[DOWNLOAD] Download complete!\n\n"
        else
            echo "[ERROR] Download of Creator-MK3 git repository failed!"
            exit -1
        fi
    else
        printf "[DOWNLOAD] Creator-MK3 repository already found locally. Continuing...\n\n"
    fi
}

#Needs more work as the file structure is not finished yet
function link_extension {
    echo "[INSTALL] Linking extension to Klipper..."
    ln -srfn "${CREATOR_MK3_PATH}/autotune_tmc.py" "${KLIPPER_PATH}/klippy/extras/autotune_tmc.py"
    ln -srfn "${CREATOR_MK3_PATH}/motor_constants.py" "${KLIPPER_PATH}/klippy/extras/motor_constants.py"
    ln -srfn "${CREATOR_MK3_PATH}/motor_database.cfg" "${KLIPPER_PATH}/klippy/extras/motor_database.cfg"
}

function restart_klipper {
    echo "[POST-INSTALL] Restarting Klipper..."
    sudo systemctl restart klipper
}


printf "\n======================================\n"
echo "- Creator-MK3s install script -"
printf "======================================\n\n"


# Run steps
preflight_checks
check_download
link_extension
restart_klipper
