# creator-mk3s-config
This repo contains the klipper config for Prusa MK3/s and does not try to maintain the compatebility of other stock Prusa implementations. This means that the stock slicer profiles will not work, see [this repo](https://gitlab.com/creator-makerspace/3dprint) for print and filament profiles ment for this setup. The aim of this config is to squeeze out as much performance as possible. Currently they are almost on par with the MK3.9/MK4, but are held back by the slightly undersized motion system. Effectivly printing parts in half the time it would take the stock setup to print the same part.


Also see [this repo](https://github.com/dz0ny/klipper-prusa-mk3s) for a more general MK3 klipper implementation

> WORD OF CAUTION: Given the increase of speed and acceleration (4x speed and 6x acceleration) the strain on the cables will be greater. So if you have not done anything with the stock termistor cables, expect them to break within as little as 100-200 hours. The motormount for the Y axis will most likely also break, but this breaks with normal use aswell. [1](https://www.printables.com/model/1063-prusa-i3-mk3-y-axis-motor-mount) | [2](https://www.printables.com/model/140036-y-axis-mount-motor-holder-upgrade-prusa-mk3-mk3s-m).



### WIFI dongles
> Linux hates some WIFI dongles, and if you are not a Linux God you might have a bad time with this.

You might also need this in order to install the WIFI driver or other dkms stuff[^1] . 
```
sudo apt-get install build-essential git dkms
sudo apt-get install raspberrypi-kernel-headers
```


 To use [dirt cheap WIFI dongles](https://www.aliexpress.com/item/4000817147623.html) based on RTL8188FTV, install following [driver](https://github.com/kelebek333/rtl8188fu).

 See also [this](https://raspberrypi.stackexchange.com/questions/43720/disable-wifi-wlan0-on-pi-3#62522) if disabling internal WIFI is needed.

 ```bash
sudo nano /boot/config.txt
```
```bash
dtoverlay=disable-wifi
#dtoverlay=disable-bt
 ```
---
[^1]: Tested and confirmed working on 6.1.21-v7+

### Spoolman[^2]
The code that goes in `printer.cfg` in order to enable QR code scanning with camera. [Shell extention](https://github.com/dw-0/kiauh/blob/master/docs/gcode_shell_command.md) for G-code are needed to run QR-code scanning. It is no problem to run `KIAUH` on a MainsailOS enviroment.
```yaml
[gcode_shell_command qrcode_qrscanner]
## NOTE: Change the path below if your klipper config is not in the default path ##
command: sh /home/pi/printer_data/config/qrcodespoolman.sh
verbose: True
timeout: 2.

[gcode_macro QRCode_Scan]
gcode:
    RUN_SHELL_COMMAND CMD=qrcode_qrscanner
```
If you do not want to use `KIAUH` to install the `Shell extention` but rather install it manually you can use the following code.
```bash
cd /home/pi/klipper/klippy/extras/
wget https://raw.githubusercontent.com/th33xitus/kiauh/master/resources/gcode_shell_command.py
```


Make the `qrcodespoolman.sh` file and place it in the config folder (the same folder as the `printer.cfg` file).
```bash
#!/bin/sh

########################################## WARNING: ##############################################
### This script assumes you are using a crowsnest webcam on the same host and the first camera ###
###      Adjust the paths and addresses below as needed to work with your configuration        ###
##################################################################################################

## Capture a snapshot from the camera and store it in a jpg file
wget http://localhost/webcam/?action=snapshot -O /home/pi/printer_data/gcodes/qrcode.jpg

## Read any QRcodes from the image and strip just the spool-id from the data in the code
SPOOLID=$(zbarimg -S*.enable /home/pi/printer_data/gcodes/qrcode.jpg | sed 's/[^0-9]*\([0-9]\+\).*/\1/')

## Return the spool-id in the console (this is mostly for debugging purposes)
echo $SPOOLID

## Make an API call to spoolman selecting the spool that matches the spool-id
curl -X POST -H "Content-Type: application/json" -d "{\"spool_id\": \"$SPOOLID\"}" http://localhost:7125/server/spoolman/spool_id
```


The code that goes in `moonraker.conf`:
```yaml
[spoolman]
server: http://10.0.0.10:7912 #<-- Change to appropriate server address
#   URL to the Spoolman instance. This parameter must be provided.
sync_rate: 5
#   The interval, in seconds, between sync requests with the
#   Spoolman server.  The default is 5.
```
[^2]: [Setup source](https://lemmy.world/post/6872890)